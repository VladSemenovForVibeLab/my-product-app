package com.myProducts.myProducts.controllers;

import com.myProducts.myProducts.exceptions.ErrorMessageException;
import com.myProducts.myProducts.service.interf.CRUDService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

public  abstract class CRUDRestController<E,K> {
    abstract CRUDService<E,K> getServiceForEntity();

    @GetMapping
    @Operation(summary = "Get all entities")
    public ResponseEntity<List<E>> getAll(){
        List<E> entities = getServiceForEntity().getAll();
        return ResponseEntity.ok(entities);
    }
    @PostMapping
    @Operation(summary = "Create entity")
    public ResponseEntity<E> save(@RequestBody E entity){
        E entitySaved = getServiceForEntity().save(entity);
        return ResponseEntity.status(HttpStatus.CREATED).body(entitySaved);
    }
    @GetMapping("/{id}")
    @Operation(summary = "Get one entity by id")
    public ResponseEntity<?> getOneById(@PathVariable K id){
        Optional<E> entityFind = getServiceForEntity().getOneEntityById(id);
        if(entityFind.isPresent()){
            return ResponseEntity.status(HttpStatus.OK).body(entityFind);
        }else{
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorMessageException("Entity not found"));
        }
    }
    @PutMapping
    @Operation(summary = "Update entity")
    public ResponseEntity<E> updateEntity(@RequestBody E entity){
        E updatedEntity = getServiceForEntity().updateEntity(entity);
        return ResponseEntity.status(HttpStatus.OK).body(updatedEntity);
    }
    @DeleteMapping("/{id}")
    @Operation(summary = "Delete entity by id")
    public ResponseEntity<Object> deleteEntity(@PathVariable K id){
        Optional<E> entity = getServiceForEntity().getOneEntityById(id);
        if(entity.isPresent()){
            getServiceForEntity().deleteEntityById(id);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body("Entity was delete");
        }else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorMessageException("Entity not found -> -_-"));
        }
    }
}
