package com.myProducts.myProducts.controllers;

import com.myProducts.myProducts.models.ProductModel;
import com.myProducts.myProducts.service.interf.CRUDService;
import com.myProducts.myProducts.service.interf.ProductService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@Tag(name = "Product Controller",description = "Product API")
@RequestMapping(ProductController.PRODUCT_REST_URL)
public class ProductController extends CRUDRestController<ProductModel, UUID> {
    public static final String PRODUCT_REST_URL="/api/v1/products";
    @Autowired
    private ProductService productService;
    @Override
    CRUDService<ProductModel, UUID> getServiceForEntity() {
        return productService;
    }
}
