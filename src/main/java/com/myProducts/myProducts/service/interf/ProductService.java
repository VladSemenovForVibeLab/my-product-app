package com.myProducts.myProducts.service.interf;

import com.myProducts.myProducts.models.ProductModel;

import java.util.UUID;

public interface ProductService extends CRUDService<ProductModel, UUID>  {
}
