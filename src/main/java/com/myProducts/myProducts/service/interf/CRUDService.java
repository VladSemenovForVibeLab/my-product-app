package com.myProducts.myProducts.service.interf;

import java.util.List;
import java.util.Optional;

public interface CRUDService<E,K> {
    List<E> getAll();

    E save (E entity);
    Optional<E> getOneEntityById(K id);

    E updateEntity(E entity);

    void deleteEntityById(K id);
}
