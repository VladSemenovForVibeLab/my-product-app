package com.myProducts.myProducts.service.implementations;

import com.myProducts.myProducts.models.ProductModel;
import com.myProducts.myProducts.repository.ProductModelRepository;
import com.myProducts.myProducts.service.interf.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class ProductServiceImplementation extends AbstractCRUDService<ProductModel, UUID> implements ProductService {

    @Autowired
    private ProductModelRepository productModelRepository;
    @Override
    JpaRepository<ProductModel, UUID> getRepositoryForEntity() {
        return productModelRepository;
    }
}
