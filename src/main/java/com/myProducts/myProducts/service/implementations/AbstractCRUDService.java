package com.myProducts.myProducts.service.implementations;

import com.myProducts.myProducts.service.interf.CRUDService;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public abstract class AbstractCRUDService<E,K> implements CRUDService<E,K> {
    abstract JpaRepository<E,K> getRepositoryForEntity();


    @Override
    @Transactional(readOnly = true)
    public List<E> getAll() {
        List<E> entities = new ArrayList<>();
        getRepositoryForEntity().findAll().forEach(entities::add);
        return entities;
    }

    @Override
    @Transactional
    public E save(E entity) {
        return getRepositoryForEntity().save(entity);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<E> getOneEntityById(K id) {
        return getRepositoryForEntity().findById(id);
    }

    @Override
    @Transactional
    public void deleteEntityById(K id) {
            getRepositoryForEntity().deleteById(id);
    }

    @Override
    @Transactional
    public E updateEntity(E entity) {
        getRepositoryForEntity().save(entity);
        return entity;
    }
}
