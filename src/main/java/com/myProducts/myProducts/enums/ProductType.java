package com.myProducts.myProducts.enums;

/**
 * Перечисление типов продуктов.
 */
public enum ProductType {
    Electronics, // Электроника
    Clothing, // Одежда
    Books, // Книги
    Furniture, // Мебель
    Beauty, // Красота
    Sports, // Спорт
    Food, // Продукты питания
    Toys, // Игрушки
    Automotive, // Автомобильные товары
    Jewelry, // Ювелирные изделия
    HomeAppliances, // Бытовая техника
    Health, // Здоровье
    Other // Другое
}