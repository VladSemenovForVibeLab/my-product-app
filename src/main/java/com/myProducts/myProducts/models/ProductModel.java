package com.myProducts.myProducts.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.myProducts.myProducts.enums.ProductType;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.UUID;

/*
 * Этот класс представляет модель продукта.
 * Он содержит информацию о продукте, такую как название, описание, тип и даты создания и обновления.
 * UUID используется в качестве уникального идентификатора продукта.
 */
@Data
@Getter
@Setter
@Entity
@Table(name = "TB_PRODUCTS")
@Schema(description = "Product model")
public class ProductModel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    @Schema(description = "Product id",example = "1")
    private UUID id;

    /*
     * Название продукта.
     * Должно быть уникальным и не может быть пустым.
     * Максимальная длина: 50 символов.
     */
    @Column(nullable = false, length = 50, unique = true)
    @Schema(description = "Product name",example = "PS6 Console")
    private String name;

    /*
     * Описание продукта.
     * Должно быть не пустым.
     * Максимальная длина: 150 символов.
     */
    @Column(nullable = false, length = 150)
    @Schema(description = "Product description",example = "Game console")
    private String description;

    /*
     * Тип продукта, представленный в виде перечисления (Enum).
     */
    @Enumerated(EnumType.STRING)
    @Schema(description = "Product type",example = "Electronics")
    private ProductType productType;

    /*
     * Дата и время создания продукта.
     * Формат JSON: "yyyy-MM-dd'T'HH:mm:ss'Z'"
     */
    @Column(nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss'Z'")
    private LocalDateTime creationDate;

    /*
     * Дата и время последнего обновления продукта.
     * Формат JSON: "yyyy-MM-dd'T'HH:mm:ss'Z'"
     */
    @Column(nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss'Z'")
    private LocalDateTime lastUpdateDate;


    @PrePersist
    protected void onCreate(){
        this.creationDate=LocalDateTime.now();
        this.lastUpdateDate=LocalDateTime.now();
    }
    @PreUpdate
    protected void onUpdate(){
        this.lastUpdateDate=LocalDateTime.now();
    }
}