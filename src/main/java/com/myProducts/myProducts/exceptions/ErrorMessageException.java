package com.myProducts.myProducts.exceptions;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@NoArgsConstructor
public class ErrorMessageException {
    private String errorMessage;
    private Map<String,String> errors;
    public ErrorMessageException(String errorMessageParam){
        this.errorMessage=errorMessageParam;
    }
}
