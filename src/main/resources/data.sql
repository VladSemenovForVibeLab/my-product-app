-- Пример 1
INSERT INTO TB_PRODUCTS (id, name, description, product_type, creation_date, last_update_date) VALUES ('f47f3ba3-69d2-4d1a-b812-29f0f9d82e79', 'PS6 Console', 'Game console', 'Electronics', '2023-09-27T12:00:00Z', '2023-09-27T12:00:00Z');

-- Пример 2
INSERT INTO TB_PRODUCTS (id, name, description, product_type, creation_date, last_update_date) VALUES ('a4dcb2af-0fe6-431e-892c-1d5c0e2579c3', 'T-shirt', 'Cotton t-shirt', 'Clothing', '2023-09-27T12:00:00Z', '2023-09-27T12:00:00Z');

-- Пример 3
INSERT INTO TB_PRODUCTS (id, name, description, product_type, creation_date, last_update_date) VALUES ('1e9fc991-84d6-4f7a-bd58-5d442c40d6fc', 'Java Programming Book', 'Learn Java programming', 'Books', '2023-09-27T12:00:00Z', '2023-09-27T12:00:00Z');

-- Пример 4
INSERT INTO TB_PRODUCTS (id, name, description, product_type, creation_date, last_update_date) VALUES ('5c1a0e68-271b-4d1b-8325-25b9c0cde1e9', 'Oak Dining Table', 'Solid oak dining table', 'Furniture', '2023-09-27T12:00:00Z', '2023-09-27T12:00:00Z');

-- Пример 5
INSERT INTO TB_PRODUCTS (id, name, description, product_type, creation_date, last_update_date) VALUES ('3691a6d5-6e2f-4f72-96a2-16e0d4771e23', 'Lipstick', 'Red lipstick', 'Beauty', '2023-09-27T12:00:00Z', '2023-09-27T12:00:00Z');

-- Пример 6
INSERT INTO TB_PRODUCTS (id, name, description, product_type, creation_date, last_update_date) VALUES ('6d66444a-eab2-4ed1-933c-3de89e932bd0', 'Basketball', 'Official size basketball', 'Sports', '2023-09-27T12:00:00Z', '2023-09-27T12:00:00Z');
-- Пример 7
INSERT INTO TB_PRODUCTS (id, name, description, product_type, creation_date, last_update_date) VALUES ('d1e4f4eb-4e64-4e16-bf5a-82bf1b53a98e', 'Apples', 'Fresh apples', 'Food', '2023-09-27T12:00:00Z', '2023-09-27T12:00:00Z');

-- Пример 8
INSERT INTO TB_PRODUCTS (id, name, description, product_type, creation_date, last_update_date) VALUES ('3b6f9b4e-4e42-4e1a-a8d0-2e751a5897e6', 'LEGO Set', 'Building toy set', 'Toys', '2023-09-27T12:00:00Z', '2023-09-27T12:00:00Z');

-- Пример 9
INSERT INTO TB_PRODUCTS (id, name, description, product_type, creation_date, last_update_date) VALUES ('f5475d36-45ca-4b4d-b84e-1c5e9d9a2d02', 'Car Oil', 'Engine oil for cars', 'Automotive', '2023-09-27T12:00:00Z', '2023-09-27T12:00:00Z');

-- Пример 10
INSERT INTO TB_PRODUCTS (id, name, description, product_type, creation_date, last_update_date) VALUES ('da7d46c7-dce5-4e6d-8659-50e73c1c51a6', 'Diamond Ring', 'High-quality diamond ring', 'Jewelry', '2023-09-27T12:00:00Z', '2023-09-27T12:00:00Z');

